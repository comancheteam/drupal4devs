ARG DRUPAL_IMAGE_VERSION
FROM drupal:${DRUPAL_IMAGE_VERSION}

LABEL maintainer="Comanche Team"

# Delete Drupal
RUN rm -fr /var/www/html/* 
#&& \
#mkdir /var/www/.composer && \
#chown -R www-data.www-data /var/www/.composer /var/www/html && \
#chmod -R 775 /var/www/.composer /var/www/html

#Install necessary packages and 
RUN set -ex \
	&& buildDeps=' \
		git \
    	vim \
		curl \
		unzip \
    	mariadb-client \
        dnsutils \
        net-tools \
		' \
	&& apt-get update && apt-get install -y --no-install-recommends $buildDeps \
  	&& rm -rf /var/lib/apt/lists/* 

#Drush install
RUN curl -sOL https://github.com/drush-ops/drush-launcher/releases/latest/download/drush.phar && \
	chmod +x drush.phar && \
	mv drush.phar /usr/local/bin/drush
	
#Composer install
RUN curl -s -o composer-setup.php https://getcomposer.org/installer
RUN php composer-setup.php && \
	mv composer.phar /usr/local/bin/composer && \
	php -r "unlink('composer-setup.php');"
