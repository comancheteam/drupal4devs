# README #

Projeto Plug 'n Play Drupal para Desenvolvedores.

## What is this repository for? ##

Código para subir um Projeto Plug 'n Play do Drupal para desenvolvimento.

## Requisitos ##
php + extensões (php-cli, php-mbstring, ext-dom, gd)
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.3 php7.3-xml php7.3-gd
php -v ##Deve ser a versão 7.3
sudo update-alternatives --config php ##Troca de versão do PHP, selecionar o php 7.3
```

composer
```
wget -O composer-setup.php https://getcomposer.org/installer
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
php -r "unlink('composer-setup.php');"
```

docker (no Ubuntu Linux)
```
https://docs.docker.com/engine/install/ubuntu/
```

docker-compose (Standalone)
```
https://docs.docker.com/compose/install/other/
```


## Orientações ##

* user e senha de administrador : admin/admin
* comandos úteis estão no arquivo - Makefile
* variáveis em estão no arquivo - .env

## Rodando ##

* Development setup - Sobe um Drupal do zero
```
make up
```

* Acessando

```
https://localhost:8080
```

* Remove todos containers
```
make down
```

* Project backup - Faz um backup do projeto
  Exporta o banco para a pasta config/dumps e 
  Exporta os arquivos de configuração do projeto para a pasta config/sync
```
make back
```

* Project setup - Sobe um projeto Drupal já instalado
  Requer que o banco esteja salvo na pasta config/dumps e que
  os arquivos de configuração do projeto estejam salvos em config/sync
```
make restore
```

* Remove todo projeto (pasta)
```
make removeproject
```
